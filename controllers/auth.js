const { response } = require('express');
const Usuario = require('../models/usuario.model');
const bcrypt = require('bcryptjs');
const { generarJWT } = require('../helpers/jwt');



const login = async (req, res) => {

    const { email, password} = req.body;

    try {

       const usuarioDB = await Usuario.findOne({email});
        // VERIFICAR CONTRASEÑA
       if(!usuarioDB) {
           return res.status(404).json({
               res:false,
               msg: 'Email no valido'
           })
       }

       // vERIFICAR CONTRASEÑA
       const validPassword = bcrypt.compareSync(password, usuarioDB.password);
       if(!validPassword) {
           return res.status(400).json({
               ok:false,
               msg: 'Contraseña no valida'
           })
       }

       // GENERAR TOKEN
       const token = await generarJWT(usuarioDB.id);



       res.json({
           ok:true,
           token
       })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrados'
        })
    }

} 

module.exports = {
    login
}