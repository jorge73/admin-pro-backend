
const { response} = require('express');
const bcrypt = require('bcryptjs');
const Usuario = require('../models/usuario.model');
const { generarJWT } = require('../helpers/jwt');

const getUsuarios = async (req, res) => {

   // const usuarios = await Usuario.find();
    const usuarios = await Usuario.find({}, 'nombre email role google password');

    res.json({
        ok:true,
        usuarios: usuarios
    });
}

const postUsuarios = async (req, res = response) => { 
    
    const { email, password } = req.body;

    try {

        const existeEmail = await Usuario.findOne({email});

        if (existeEmail) {
            return res.status(400).json({
                ok: false,
                msg: 'El correo ya está registrado'
            });
        }

        const usuario = new Usuario(req.body);

        // Encriptar contraseña
        const salt = bcrypt.genSaltSync();
        usuario.password = bcrypt.hashSync(password, salt);

        // Guardar Usuario
        await usuario.save();

        //Generar JWT -token
        const token = await generarJWT(usuario.id);
    
        res.json({
            ok:true,
            usuario: usuario,
            token
        });   
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg: 'Error inesperado...revisar logs'
        })
    }
}

const putUsuarios = async ( req, res = response) => {

    const uid = req.params.id;

    try {

        const usuarioDB = await Usuario.findById(uid);

        if (!usuarioDB){
            return res.status(404).json({
                ok: false,
                msg: 'No existe un usuario por ese id'
            })
        }
        // Actualizaciones destructuracion para lo campos que quiero actualizar y comprobar que no existe
        const {password, google, email, ...campos} = req.body;

        if( usuarioDB.email !== email){

            const existeEmail = await Usuario.findOne({ email: email });
            if( existeEmail ) {
                return res.status(400).json({
                    ok: false,
                    msg: ' Ya existe un usuario con ese email'
                })
            }
        }

        campos.email = email;

        const usuarioActualizado = await Usuario.findByIdAndUpdate(uid, campos, {new: true});
        
        res.json({
            ok: true,
            usuario: usuarioActualizado
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado' 
        })
    }
}

const deleteUsuario = async( req, res = response) => {

    const uid = req.params.id;

    try {

        const usuarioDB = await Usuario.findById(uid);

        if (!usuarioDB){
            return res.status(404).json({
                ok: false,
                msg: 'No existe un usuario por ese id'
            })
        }
        await Usuario.findByIdAndDelete(uid);

        res.json({
            ok: true,
            msg: 'No existe un usuario por es id'
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

module.exports = {
    getUsuarios,
    postUsuarios,
    putUsuarios,
    deleteUsuario
}