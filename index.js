

const express = require('express');
var cors = require('cors');
require('dotenv').config();

const { dbConnection } = require('./database/config');

// Crear el servidor de express
 const app = express(); 

 // Configuración cors
 app.use(cors());

 // Lectura y parseo del body

 app.use( express.json());

 // Base de Datos
  dbConnection();

console.log(process.env);

// Rutas
app.use('/api/usuarios', require('./routes/usuarios.routes'));
app.use('/api/login', require('./routes/auth'));


 app.listen( process.env.PORT, () => {
    console.log('Servidor corriendo!!!' + process.env.PORT);
 });